/*eslint-env browser*/
// la primera línea es para eliminar los errores que da brackets por usar eslint
// https://eslint.org/docs/rules/no-undef 
/* una lista de eventos se encuentra en https://www.w3schools.com/jsref/dom_obj_event.asp */

document.getElementById("btnAgregar").addEventListener("click", enviarPeticion);
document.getElementById("popup-cerrar").addEventListener("click", cerrar);

var getParams = function (url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};

id = getParams(window.location.href);
pintarItem();

function enviarPeticion(){
    if (id.var != null) {
        editarItem();
    }else{
        agregarItem();
    }
}

function agregarItem() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var respuesta = xhr.responseXML;
            var x = respuesta.getElementsByTagName("resultado");
            document.getElementsByClassName("popup-titulo")[0].innerHTML = x[0].getElementsByTagName("titulo")[0].textContent;
            document.getElementsByClassName("popup-contenido")[0].innerHTML = x[0].getElementsByTagName("contenido")[0].textContent;

            var colorTitulo = document.getElementById("titulo").innerText;
            if (colorTitulo == "Item agregado correctamente") {
                document.getElementById("cabecera").style.backgroundColor = "#5cb85c"
                document.getElementsByClassName("popup-contenido")[0].innerHTML = "Item: " + x[0].getElementsByTagName("contenido")[0].textContent;
            } else {
                document.getElementById("cabecera").style.backgroundColor = "#cd1818"
            }
            document.getElementById("popup").style.display = "block";
        }
    };

    var nombre = document.getElementById("nombre").value;
    var modelo = document.getElementById("modelo").value;
    var marca = document.getElementById("marca").value;
    var dimensiones = document.getElementById("dimensiones").value;
    var color = document.getElementById("color").value;
    var fecha = document.getElementById("fecha").value;
    var descripcion = document.getElementById("descripcion").value;
    var q = "nombre=" + nombre + "&" + "modelo=" + modelo + "&" + "marca=" + marca + "&" + "dimensiones=" + dimensiones + "&" + "color=" + color + "&" + "fecha=" + fecha + "&" + "descripcion=" + descripcion;

    xhr.open("POST", "http://127.0.0.1:8085/cgi-bin/insertarItem.pl", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(q);
}

function editarItem() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var respuesta = xhr.responseXML;
            var x = respuesta.getElementsByTagName("resultado");
            document.getElementsByClassName("popup-titulo")[0].innerHTML = x[0].getElementsByTagName("titulo")[0].textContent;
            document.getElementsByClassName("popup-contenido")[0].innerHTML = x[0].getElementsByTagName("contenido")[0].textContent;

            var colorTitulo = document.getElementById("titulo").innerText;
            if (colorTitulo == "Item actualizado correctamente") {
                document.getElementById("cabecera").style.backgroundColor = "#5cb85c"
                document.getElementsByClassName("popup-contenido")[0].innerHTML = "Item: " + x[0].getElementsByTagName("contenido")[0].textContent;
            } else {
                document.getElementById("cabecera").style.backgroundColor = "#cd1818"
            }
            document.getElementById("popup").style.display = "block";
        }
    };

    var nombre = document.getElementById("nombre").value;
    var modelo = document.getElementById("modelo").value;
    var marca = document.getElementById("marca").value;
    var dimensiones = document.getElementById("dimensiones").value;
    var color = document.getElementById("color").value;
    var fecha = document.getElementById("fecha").value;
    var descripcion = document.getElementById("descripcion").value;
    var q = "nombre=" + nombre + "&" + "modelo=" + modelo + "&" + "marca=" + marca + "&" + "dimensiones=" + dimensiones + "&" + "color=" + color + "&" + "fecha=" + fecha + "&" + "descripcion=" + descripcion + "&" + "var=" + id.var;

    xhr.open("POST", "http://127.0.0.1:8085/cgi-bin/editarItem.pl", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(q);
}

function cerrar() {
    document.getElementById("popup").style.display = "none";
    var titulo = document.getElementById("titulo").innerText;
    if (titulo == "Item agregado correctamente" || titulo == "Item actualizado correctamente") {
        window.location = "items.html";
    }
}

function pintarItem() {
    if (id.var != null) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                var respuesta = xhr.responseXML;
                var x = respuesta.getElementsByTagName("resultado");
                document.getElementById("nombre").value = x[0].getElementsByTagName("nombre")[0].textContent;
                document.getElementById("modelo").value = x[0].getElementsByTagName("modelo")[0].textContent;
                document.getElementById("marca").value = x[0].getElementsByTagName("marca")[0].textContent;
                document.getElementById("dimensiones").value = x[0].getElementsByTagName("dimensiones")[0].textContent;
                document.getElementById("color").value = x[0].getElementsByTagName("color")[0].textContent;
                document.getElementById("fecha").value = x[0].getElementsByTagName("fecha")[0].textContent;
                document.getElementById("descripcion").value = x[0].getElementsByTagName("descripcion")[0].textContent;
            }
        };
        xhr.open("GET", "http://127.0.0.1:8085/cgi-bin/leerItembyId.pl?var=" + id.var, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send();
    }
}
