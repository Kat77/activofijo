/*eslint-env browser*/
// la primera línea es para eliminar los errores que da brackets por usar eslint
// https://eslint.org/docs/rules/no-undef 
/* una lista de eventos se encuentra en https://www.w3schools.com/jsref/dom_obj_event.asp */

enviarPeticion();

function enviarPeticion() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var respuesta = xhr.responseXML;

            var contenedor = document.getElementById("contenedor");
            // Obtiene la tabla y crea un elemento <tbody>
            var tabla = document.getElementById("tablePrincipal");
            var tblBody = document.createElement("tbody");

            var x = respuesta.getElementsByTagName("resultado");

            //Con esto sabemos el numero de elementos que vienen en la respuesta
            var noNull = x[0].getElementsByTagName("titulo")[0];
            var i = 0;
            while (noNull !== undefined) {
                noNull = x[0].getElementsByTagName("titulo")[i];
                i++;
            }

            var numItems = i - 1;
            
            // Crea las celdas
            for (var i = 0; i < numItems; i++) {
                // Crea las filas de la tabla
                var fila = document.createElement("tr");
                // Crea un elemento <td> y un nodo de texto, hace que el nodo de
                // texto sea el contenido de <td>, ubica el elemento <td> al final
                // de la fila de la tabla
                var celdaNombre = document.createElement("td");
                var textoNombre = document.createTextNode(x[0].getElementsByTagName("nombre")[i].textContent);

                var celdaModelo = document.createElement("td");
                var textoModelo = document.createTextNode(x[0].getElementsByTagName("modelo")[i].textContent);

                var celdaMarca = document.createElement("td");
                var textMarca = document.createTextNode(x[0].getElementsByTagName("marca")[i].textContent);

                var celdaFecha = document.createElement("td");
                var textoFecha = document.createTextNode(x[0].getElementsByTagName("fecha")[i].textContent);
                
                //Se crea botón VerItem
                var celdaOpciones = document.createElement("td");
                var botonOpciones = document.createElement("button");
                var imgOpciones = document.createElement("img");
                //Icono de opciones
                imgOpciones.setAttribute("width", "22");
                imgOpciones.setAttribute("height", "22");
                imgOpciones.setAttribute("alt", "Icono Ver Item");
                imgOpciones.setAttribute("src", "img/verItem.png");
                
                //Se le asigna la clase
                botonOpciones.setAttribute("class", "button buttonItem buttonVerItem");
                //Se recupera el id del item (de la base de datos)
                var idItem = (x[0].getElementsByTagName("id")[i].textContent)
                //Se le asigna ese id al value y id del button
                botonOpciones.setAttribute("value", idItem);
                botonOpciones.setAttribute("id", idItem);
                botonOpciones.setAttribute("onclick", "verItem(this)");
                //Se pinta "Ver Item" en el button
                botonOpciones.innerText = 'Ver Item';
                                
                celdaNombre.appendChild(textoNombre);
                celdaModelo.appendChild(textoModelo);
                celdaMarca.appendChild(textMarca);
                celdaFecha.appendChild(textoFecha);
                botonOpciones.appendChild(imgOpciones);
                celdaOpciones.appendChild(botonOpciones);

                fila.appendChild(celdaNombre);
                fila.appendChild(celdaModelo);
                fila.appendChild(celdaMarca);
                fila.appendChild(celdaFecha);
                fila.appendChild(celdaOpciones);

                // agrega la fila al final de la tabla (al final del elemento tblbody)
                tblBody.appendChild(fila);
            }

            // posiciona el <tbody> debajo de la tabla
            tabla.appendChild(tblBody);
            // agrega la tabla dentro del contenedor(es decir, del div Contenedor)
            contenedor.appendChild(tabla);
        }
    };

    xhr.open("GET", "http://127.0.0.1:8085/cgi-bin/leerItem.pl", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();
}

function verItem(boton){
    var itemId = boton.value;
    window.location="verItem.html?var="+itemId;
}