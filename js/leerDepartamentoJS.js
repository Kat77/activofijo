/*eslint-env browser*/
// la primera línea es para eliminar los errores que da brackets por usar eslint
// https://eslint.org/docs/rules/no-undef 
/* una lista de eventos se encuentra en https://www.w3schools.com/jsref/dom_obj_event.asp */

enviarPeticion();

function enviarPeticion() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var respuesta = xhr.responseXML;

            var contenedor = document.getElementById("contenedor");
            // Obtiene la tabla y crea un elemento <tbody>
            var tabla = document.getElementById("tablePrincipal");
            var tblBody = document.createElement("tbody");

            var x = respuesta.getElementsByTagName("resultado");

            var noNull = x[0].getElementsByTagName("titulo")[0];
            var i = 0;
            while (noNull !== undefined) {
                noNull = x[0].getElementsByTagName("titulo")[i];
                i++;
            }

            var numDepartamentos = i - 1;
            // Crea las celdas, aqui la i representa el numero de filas
            for (var i = 0; i < numDepartamentos; i++) {
                // Crea las filas de la tabla
                var fila = document.createElement("tr");
                // Crea un elemento <td> y un nodo de texto, hace que el nodo de
                // texto sea el contenido de <td>, ubica el elemento <td> al final
                // de la fila de la tabla
                var celdaNombre = document.createElement("td");
                var textoNombre = document.createTextNode(x[0].getElementsByTagName("nombre")[i].textContent);

                var celdaPiso = document.createElement("td");
                var textoPiso = document.createTextNode(x[0].getElementsByTagName("piso")[i].textContent);

                var celdaZona = document.createElement("td");
                var textoZona = document.createTextNode(x[0].getElementsByTagName("zona")[i].textContent);

                //Se crea botón VerDepartamento
                var celdaOpciones = document.createElement("td");
                var botonOpciones = document.createElement("button");
                var imgOpciones = document.createElement("img");
                //Icono de opciones
                imgOpciones.setAttribute("width", "22");
                imgOpciones.setAttribute("height", "22");
                imgOpciones.setAttribute("alt", "Icono Ver Empleado");
                imgOpciones.setAttribute("src", "img/verDepartamento.png");
                
                //Se le asigna la clase
                botonOpciones.setAttribute("class", "button buttonDepartamento buttonVerDepartamento");
                //Se recupera el id del departamento (de la base de datos)
                var idDepartamento = (x[0].getElementsByTagName("id")[i].textContent)
                //Se le asigna ese id al value y id del button
                botonOpciones.setAttribute("value", idDepartamento);
                botonOpciones.setAttribute("id", idDepartamento);
                botonOpciones.setAttribute("onclick", "verDepartamento(this)");
                //Se pinta "Ver Departamento" en el button
                botonOpciones.innerText = 'Ver Departamento';

                celdaNombre.appendChild(textoNombre);
                celdaPiso.appendChild(textoPiso);
                celdaZona.appendChild(textoZona);
                botonOpciones.appendChild(imgOpciones);
                celdaOpciones.appendChild(botonOpciones);

                fila.appendChild(celdaNombre);
                fila.appendChild(celdaPiso);
                fila.appendChild(celdaZona);
                fila.appendChild(celdaOpciones);

                // agrega la fila al final de la tabla (al final del elemento tblbody)
                tblBody.appendChild(fila);
            }

            // posiciona el <tbody> debajo de la tabla
            tabla.appendChild(tblBody);
            // agrega la tabla dentro del contenedor(es decir, del div Contenedor)
            contenedor.appendChild(tabla);
        }
    };

    xhr.open("GET", "http://127.0.0.1:8085/cgi-bin/leerDepartamento.pl", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();
}

function verDepartamento(boton) {
    var idDepartamento = boton.value;
    window.location="verDepartamento.html?var="+idDepartamento;
}
