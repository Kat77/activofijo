/*eslint-env browser*/
// la primera línea es para eliminar los errores que da brackets por usar eslint
// https://eslint.org/docs/rules/no-undef 
/* una lista de eventos se encuentra en https://www.w3schools.com/jsref/dom_obj_event.asp */
document.getElementById("btnEliminar").addEventListener("click", eliminarEmpleado);
document.getElementById("btnEditar").addEventListener("click", editarEmpleado);
document.getElementById("popup-cerrar").addEventListener("click", cerrar);

var getParams = function (url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};

id = getParams(window.location.href);
enviarPeticion();

function enviarPeticion() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var respuesta = xhr.responseXML;
            var x = respuesta.getElementsByTagName("resultado");
            document.getElementById("NombreEmpleado").innerHTML = x[0].getElementsByTagName("nombre")[0].textContent;
            document.getElementById("Cargo").innerHTML = x[0].getElementsByTagName("cargo")[0].textContent;
            document.getElementById("Fecha").innerHTML = x[0].getElementsByTagName("fecha")[0].textContent;
        }
    };
    xhr.open("GET", "http://127.0.0.1:8085/cgi-bin/leerEmpleadobyId.pl?var=" + id.var, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();
}

function eliminarEmpleado() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var respuesta = xhr.responseXML;
            var x = respuesta.getElementsByTagName("resultado");
            document.getElementsByClassName("popup-titulo")[0].innerHTML = x[0].getElementsByTagName("titulo")[0].textContent;
            document.getElementsByClassName("popup-contenido")[0].innerHTML = x[0].getElementsByTagName("contenido")[0].textContent;

            var colorTitulo = document.getElementById("titulo").innerText;
            if (colorTitulo == "Empleado eliminado correctamente") {
                document.getElementById("cabecera").style.backgroundColor = "#5cb85c"
                document.getElementsByClassName("popup-contenido")[0].innerHTML = "ID del empleado: " + x[0].getElementsByTagName("contenido")[0].textContent;
            } else {
                document.getElementById("cabecera").style.backgroundColor = "#cd1818"
            }
            document.getElementById("popup").style.display = "block";
        }
    };
    var q = "var=" + id.var ;
    xhr.open("POST", "http://127.0.0.1:8085/cgi-bin/eliminarEmpleado.pl", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(q);
}

function cerrar() {
    document.getElementById("popup").style.display = "none";
    var titulo = document.getElementById("titulo").innerText;
    if (titulo == "Empleado eliminado correctamente") {
        window.location = "empleados.html";
    }
}

function editarEmpleado(){
    var itemId = id.var
    window.location="aeEmpleados.html?var="+id.var;
}