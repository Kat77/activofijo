/*eslint-env browser*/
// la primera línea es para eliminar los errores que da brackets por usar eslint
// https://eslint.org/docs/rules/no-undef 
/* una lista de eventos se encuentra en https://www.w3schools.com/jsref/dom_obj_event.asp */

document.getElementById("btnAgregar").addEventListener("click", enviarPeticion);
document.getElementById("popup-cerrar").addEventListener("click", cerrar);

var getParams = function (url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};

id = getParams(window.location.href);
pintarEmpleado();

function enviarPeticion(){
    if (id.var != null) {
        editarDepartamento();
    }else{
        agregarDepartamento();
    }
}

function agregarDepartamento() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var respuesta = xhr.responseXML;
            var x = respuesta.getElementsByTagName("resultado");
            document.getElementsByClassName("popup-titulo")[0].innerHTML = x[0].getElementsByTagName("titulo")[0].textContent;
            document.getElementsByClassName("popup-contenido")[0].innerHTML = x[0].getElementsByTagName("contenido")[0].textContent;
            
            var colorTitulo = document.getElementById("titulo").innerText;
            if (colorTitulo == "Departamento agregado correctamente") {
                document.getElementById("cabecera").style.backgroundColor = "#5cb85c"
                document.getElementsByClassName("popup-contenido")[0].innerHTML = "Departamento: " + x[0].getElementsByTagName("contenido")[0].textContent;
            }else{
                document.getElementById("cabecera").style.backgroundColor = "#cd1818"
            }
            document.getElementById("popup").style.display = "block";
        }
    };

    var nombre = document.getElementById("nombre").value;
    var piso = document.getElementById("piso").value;
    var zona = document.getElementById("zona").value;
    var q = "nombre=" + nombre + "&" + "piso=" + piso + "&" + "zona=" + zona ;

    xhr.open("POST", "http://127.0.0.1:8085/cgi-bin/insertarDepartamento.pl", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(q);
}

function editarDepartamento() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var respuesta = xhr.responseXML;
            var x = respuesta.getElementsByTagName("resultado");
            document.getElementsByClassName("popup-titulo")[0].innerHTML = x[0].getElementsByTagName("titulo")[0].textContent;
            document.getElementsByClassName("popup-contenido")[0].innerHTML = x[0].getElementsByTagName("contenido")[0].textContent;

            var colorTitulo = document.getElementById("titulo").innerText;
            if (colorTitulo == "Departamento actualizado correctamente") {
                document.getElementById("cabecera").style.backgroundColor = "#5cb85c"
                document.getElementsByClassName("popup-contenido")[0].innerHTML = "Empleado: " + x[0].getElementsByTagName("contenido")[0].textContent;
            } else {
                document.getElementById("cabecera").style.backgroundColor = "#cd1818"
            }
            document.getElementById("popup").style.display = "block";
        }
    };

    var nombre = document.getElementById("nombre").value;
    var piso = document.getElementById("piso").value;
    var zona = document.getElementById("zona").value;
    var q = "nombre=" + nombre + "&" + "piso=" + piso + "&" + "zona=" + zona + "&" + "var=" + id.var;

    xhr.open("POST", "http://127.0.0.1:8085/cgi-bin/editarDepartamento.pl", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(q);
}

function cerrar() {
    document.getElementById("popup").style.display = "none";
    var titulo = document.getElementById("titulo").innerText;
    if (titulo == "Departamento agregado correctamente" || titulo == "Departamento actualizado correctamente") {
        window.location = "departamentos.html";
    }
}

function pintarEmpleado() {
    if (id.var != null) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                var respuesta = xhr.responseXML;
                var x = respuesta.getElementsByTagName("resultado");
                document.getElementById("nombre").value = x[0].getElementsByTagName("nombre")[0].textContent;
                document.getElementById("piso").value = x[0].getElementsByTagName("piso")[0].textContent;
                document.getElementById("zona").value = x[0].getElementsByTagName("zona")[0].textContent;
            }
        };
        xhr.open("GET", "http://127.0.0.1:8085/cgi-bin/leerDepartamentobyId.pl?var=" + id.var, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send();
    }
}
